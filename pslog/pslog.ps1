﻿#
# $Id: pslog.ps1, v1.0.0 09/04/2015 20:00:00 $
#

param (
	[Switch]$debug = $false
)

#Environment
$LogDLL = "C:\pslog\lib\log4net\bin\net\2.0\log4net.dll"
$LogConfig = "C:\pslog\pslog.config"

#RunID
$runId = (Get-Date -Format "yyyyMMddHHmmss") + (Get-Date).Millisecond

#Define logger
function Get-Logger() {
	<# 
		.SYNOPSIS 
			This function creates a log4net logger instance already configured 
		
		.OUTPUTS 
			The log4net logger instance ready to be used
	#> 
	
	[CmdletBinding()] 
	param (
		#Path of the configuration file of log4net 
		[String]$Configuration,
		#Log4net dll path
		[Alias("Dll")] [String]$log4netDllPath
	)
	
	Write-Verbose "[Get-Logger] Logger initialization"
	
	$log4netDllPath = Resolve-Path $log4netDllPath -ErrorAction SilentlyContinue -ErrorVariable Err
	if ($Err) { 
		throw "Log4net library cannot be found on the path $log4netDllPath" 
	} else { 
		Write-Verbose "[Get-Logger] Log4net dll path is '$log4netDllPath'"
		[void][Reflection.Assembly]::LoadFrom($log4netDllPath) | Out-Null   
		
		#Log4net configuration loading
		$log4netConfigFilePath = Resolve-Path $Configuration -ErrorAction SilentlyContinue -ErrorVariable Err
		if ($Err) {
			throw "Log4Net configuration file $Configuration cannot be found" 
		} else {
			Write-Verbose "[Get-Logger] Log4net configuration file is '$log4netConfigFilePath'" 
			$FileInfo = New-Object System.IO.FileInfo($log4netConfigFilePath)
			
			[log4net.Config.XmlConfigurator]::Configure($FileInfo)
			$script:loggerObj = [log4net.LogManager]::GetLogger("root")
			
			Write-Verbose "[Get-Logger] Logger is configured"
			
			return $loggerObj
		}
	}
}

#Create file appender logger
if ($debug) {
	$log = Get-Logger -Configuration $LogConfig -Dll $LogDLL -verbose
} else {
	$log = Get-Logger -Configuration $LogConfig -Dll $LogDLL
}

#
# ... code step 1 ...
#
$log.Info("[${runId} - step 1] execution was successful")

Start-Sleep -Seconds 10

#
# ... code step 2 ...
#
$log.Warn("[${runId} - step 2] attention something is not correct")

Start-Sleep -Seconds 10

#
# ... code step 3 ...
#
$log.Error("[${runId} - step 3] execution failed")
